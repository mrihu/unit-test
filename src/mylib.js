/** Basic arithmetic operations */
const mylib = {
    add: (a, b) => {
        /** Multiline arrow function */
        const sum = a + b;
        return sum;
    },
    subtract: (a, b) => {
        return a - b;
    },
    /** Multiline arrow function with if statement */
    divide: (dividend, divisor) => {if((divisor) == 0) {
        return "Error";
        } else {
        return dividend / divisor;
        }
    },
    /** Regular function */
    multiply: function(a, b) {
        return a * b;
    }
};

module.exports = mylib;