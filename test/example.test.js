const expect = require('chai').expect;
const mylib = require('../src/mylib');

describe("Our first unit tests", () => {
    before(() => {
        // initializon
        // create objects... etc...
        console.log("Initialising tests");
    });
    it("Can add 1 and 2 together", () => {
        expect(mylib.add(1, 2)).equal(3, "1 + 2 is not 3, for some reason?");
    });
    it("Returns \"Error\" if divider is zero", () => {
        expect(mylib.divide(8, 0)).equal("Error", "Divider is zero")
    });
    it("Subtraction can be minus", () => {
        expect(mylib.subtract(2, 10)).equal(-8, "2 - 10 is not -8, for some reason?");
    });
    it("Multiplication works with two factors with a minus sign", () => {
        expect(mylib.multiply(-4, -10)).above(0, "-4 * -10 is not positive, for some reason");
    });
    it("Multiplication works with one factor with a minus sign", () => {
        expect(mylib.multiply(-4, 10)).below(0, "-4 * 10 is positive, for some reason");
    });
    after(() => {
        // Cleanup
        // For example: shut the Express server.
        console.log("Testing completed!")
    });
});